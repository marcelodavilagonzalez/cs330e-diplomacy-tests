#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Support E\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1

coverage report -m                      >> TestDiplomacy.tmp

cat TestDiplomacy.tmp
...
----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          64      0     42      0   100%
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                 85      0     42      0   100%

"""
